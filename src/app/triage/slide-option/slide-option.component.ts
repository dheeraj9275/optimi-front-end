import { Component, OnInit } from "@angular/core";
import { Platform } from "@ionic/angular";

@Component({
  selector: "app-slide-option",
  templateUrl: "./slide-option.component.html",
  styleUrls: ["./slide-option.component.scss"],
})
export class SlideOptionComponent implements OnInit {
  heightVal: number;
  weightVal: number;
  constructor(public plateform: Platform) {
    this.plateform.ready().then(() => {
      this.heightVal = 0;
      this.weightVal = 0;
    });
  }

  ngOnInit() {}
}
