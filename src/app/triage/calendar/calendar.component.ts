import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.scss"],
})
export class CalendarComponent implements OnInit {
  mytime: Date = new Date();
  dateTime = new FormGroup({
    notify: new FormControl(),
    time: new FormControl(),
    allDay: new FormControl(),
  });
  constructor() {}

  ngOnInit() {}
  onSubmit() {
    console.log(this.dateTime.value);
  }
}
