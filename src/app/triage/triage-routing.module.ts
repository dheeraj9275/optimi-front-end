import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CalendarComponent } from "./calendar/calendar.component";
import { QuestionnaireComponent } from "./questionnaire/questionnaire.component";
import { SlideOptionComponent } from "./slide-option/slide-option.component";
import { TriagePlanComponent } from "./triage-plan/triage-plan.component";

import { TriagePage } from "./triage.page";
import { WelcomeComponent } from "./welcome/welcome.component";

const routes: Routes = [
  {
    path: "",
    component: WelcomeComponent,
  },
  {
    path: "welcome",
    component: WelcomeComponent,
  },
  {
    path: "slide-option",
    component: SlideOptionComponent,
  },
  {
    path: "questions",
    component: QuestionnaireComponent,
  },
  {
    path: "plan",
    component: TriagePlanComponent,
  },
  {
    path: "calendar",
    component: CalendarComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TriagePageRoutingModule {}
