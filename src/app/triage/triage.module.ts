import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { TriagePageRoutingModule } from "./triage-routing.module";
import { TriagePage } from "./triage.page";
import { WelcomeComponent } from "./welcome/welcome.component";
import { SlideOptionComponent } from "./slide-option/slide-option.component";
import { MatSliderModule } from "@angular/material/slider";
import { QuestionnaireComponent } from "./questionnaire/questionnaire.component";
import { TriagePlanComponent } from "./triage-plan/triage-plan.component";
import { SwiperModule } from "swiper/angular";
import { CalendarComponent } from "./calendar/calendar.component";
import { TimepickerModule } from "ngx-bootstrap/timepicker";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TriagePageRoutingModule,
    MatSliderModule,
    SwiperModule,
    ReactiveFormsModule,
    TimepickerModule,
  ],
  declarations: [
    TriagePage,
    WelcomeComponent,
    SlideOptionComponent,
    QuestionnaireComponent,
    TriagePlanComponent,
    CalendarComponent,
  ],
})
export class TriagePageModule {
  constructor() {
    console.log("Triage Module loaded");
  }
}
