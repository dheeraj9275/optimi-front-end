import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-questionnaire",
  templateUrl: "./questionnaire.component.html",
  styleUrls: ["./questionnaire.component.scss"],
})
export class QuestionnaireComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  questions = [
    {
      question:
        "Do you ever have chest pain when performing physical activity?",
      options: ["Yes", "No"],
    },
  ];
}
