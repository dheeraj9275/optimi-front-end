import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PlanPageRoutingModule } from './plan-routing.module';
import { PlanPage } from './plan.page';
import {MatStepperModule} from '@angular/material/stepper'

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, PlanPageRoutingModule,MatStepperModule],
  declarations: [PlanPage],
})
export class PlanPageModule {
  constructor() {
    console.log('plan Module loaded');
  }
}
