import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  public appMenu = [{ title: "test", url: "", icon: "list" }];
  constructor() {}
}
