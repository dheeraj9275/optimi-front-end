import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      {
        path: 'home',
        loadChildren: () =>
          import('../home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'plan',
        loadChildren: () =>
          import('../plan/plan.module').then((m) => m.PlanPageModule),
      },
      {
        path: 'expert',
        loadChildren: () =>
          import('../expert/expert.module').then((m) => m.ExpertPageModule),
      },
      {
        path: 'forum',
        loadChildren: () =>
          import('../forum/forum.module').then((m) => m.ForumPageModule),
      },
      {
        path: 'learn',
        loadChildren: () =>
          import('../learn/learn.module').then((m) => m.LearnPageModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
