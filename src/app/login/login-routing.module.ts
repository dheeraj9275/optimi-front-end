import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserLoginComponent } from "./user-login/user-login.component";
import { UserRegistrationComponent } from "./user-registration/user-registration.component";

const routes: Routes = [
  {
    path: "",
    component: UserLoginComponent,
  },
  {
    path: "user-registration",
    component: UserRegistrationComponent,
  },
  {
    path:'login',
    component:UserLoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginPageRoutingModule {
  constructor() {
    console.log("login Module loaded");
  }
}
