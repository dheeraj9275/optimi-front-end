import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";

@Component({
  selector: "app-user-registration",
  templateUrl: "./user-registration.component.html",
  styleUrls: ["./user-registration.component.scss"],
})
export class UserRegistrationComponent implements OnInit {
  userData = new FormGroup({
    fname: new FormControl("", Validators.required),
    lname: new FormControl("", Validators.required),
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(16),
    ]),
    otp: new FormControl("", [Validators.required,Validators.minLength(5)]),
    privacyPolicy:new FormControl(Validators.required),
    healthData:new FormControl(Validators.required),
    receiveUpdate:new FormControl()

  });

  constructor() {}

  ngOnInit() {}

  onSubmit() {
    console.log(this.userData.value);
  }

}
