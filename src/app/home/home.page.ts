import { Component, TemplateRef } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { LearnWithUsComponent } from "./learn-with-us/learn-with-us.component";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  constructor(private modalCtr:ModalController){}
async openModal(){
  const modal = await this.modalCtr.create({
    component:LearnWithUsComponent
  })
  return await modal.present();
}
}
