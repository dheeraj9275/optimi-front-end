import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-learn-with-us',
  templateUrl: './learn-with-us.component.html',
  styleUrls: ['./learn-with-us.component.scss'],
})
export class LearnWithUsComponent implements OnInit {

  constructor(private modalCtr:ModalController) { }

  ngOnInit() {}
  dismissModal(){
    // console.log("dismiss")
    this.modalCtr.dismiss();
  }
}
