import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';
import { LearnWithUsComponent } from './learn-with-us/learn-with-us.component';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path: 'learn-with-us',
    component: LearnWithUsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule { }
