import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { HomePage } from "./home.page";

import { HomePageRoutingModule } from "./home-routing.module";
import { ModalModule } from "ngx-bootstrap/modal";
import { NgCircleProgressModule } from 'ng-circle-progress';
import { LearnWithUsComponent } from "./learn-with-us/learn-with-us.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ModalModule,
    NgCircleProgressModule.forRoot({
      "radius": 60,
      "space": -10,
      "outerStrokeGradient": true,
      "outerStrokeWidth": 10,
      "outerStrokeColor": "#eaecff",
      "outerStrokeGradientStopColor": "#107c76",
      "innerStrokeColor": "#107c76",
      "innerStrokeWidth": 14,
      "animateTitle": true,
      "subtitle":"Weekly progress",
      
      "animationDuration": 1000,
      "showUnits": false,
      "showBackground": false,
      "clockwise": true,
      "startFromZero": false
      })
  ],
  declarations: [HomePage,LearnWithUsComponent],
})
export class HomePageModule {
  constructor() {
    console.log("Home Module loaded");
  }
}
